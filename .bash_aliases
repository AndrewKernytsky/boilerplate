# User specific aliases and functions
alias sai='sudo aptitude install'
alias sas='sudo aptitude search'
alias sa='sudo aptitude'
alias sap='sudo apt-get'

alias o="ls -ltrG  --block-size=\"'1\""
alias y='xsel -c'
alias s='xsel -p'
alias pc='pwd | xsel -c'
alias kx='killall xsel'
alias c='cat'
alias e='emacs'
alias h='head'
alias j='jobs'
alias l='echo wrong'
alias p='pbcopy'
alias m='less'
alias mo='more'
alias t='tail'
alias g='grep --color=ALWAYS'
alias q='gqview'
alias w='wc -l'
alias y='history'

alias pd='pushd'

alias eb="emacs ~/.bashrc"
alias sb="source ~/.bashrc"
alias ep="emacs ~/.profile"
alias sp="source ~/.profile"

alias du="du -h"
alias top="top i"
alias la='ls -la'
alias lh="ls -lh"
alias lo='ls -l'
alias lm='ls -l --block-size=1M -s'
alias cl='rm *~'

alias stop='kill -STOP' 
alias se='qsub -t 1-40 ~/enzyme/enzyme_batch.sh'
alias nh='nohup ~/enzyme/ga_enzyme &> run_log &'
alias cx='chmod a+x'
alias hg='history | grep --color=ALWAYS'
alias pg='ps ax | grep --color=ALWAYS'
alias ag='alias | grep --color=ALWAYS'
alias eg='env | grep --color=ALWAYS'
alias mg='mount | grep --color=ALWAYS'

alias show_hidden='shopt -s dotglob'
alias unshow_hidden='shopt -u dotglob'

# Change prompt
export PS1="\[\[\e[0;31m\][\! \[\e[0;34m\]\D{%m.%d}-\t \[\e[0;31m\]\h:\[\e[0;34m\]\W]\$\[\e[m\] " # \h:\W gives host:path

# Define colors well 
export LS_COLORS='no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.ogg=01;35:*.mp3=01;35:*.wav=01;35:';

